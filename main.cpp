// Task 4:
//     - Write a multi-threaded program that will multiply randomly generated matricies
//     - bonus:
//         - use dynamic workload balancing, analyze effectiveness.
// note:
//     - I did reuse my code from other course
// Comment: bonus task is done!
// Author: Gazizov Yakov

#include <chrono>
#include <cstdio>
#include <memory>
#include <random>
#include <exception>
#include <iostream>
#include <omp.h>

// helper class to get random numbers
class Random {
public:
    static float getFloat(float const min = 0.0f, float const max = 1.0f)
    {
        return m_distribution(m_gen) * (max - min) + min;
    }

private:
    static std::mt19937 m_gen;
    static std::uniform_real_distribution<float> m_distribution;
};
std::mt19937 Random::m_gen = std::mt19937(std::random_device()());
std::uniform_real_distribution<float> Random::m_distribution = std::uniform_real_distribution<float>(0.0f, 1.0f);

// helper class to measure time
class Timer {
public:
    static void start()
    {
        m_start = std::chrono::system_clock::now();
    }

    static auto stop()
    {
        m_stop = std::chrono::system_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(m_stop - m_start);
        return duration.count();
    }
private:
    static std::chrono::time_point<std::chrono::system_clock> m_start;
    static std::chrono::time_point<std::chrono::system_clock> m_stop;
};
std::chrono::time_point<std::chrono::system_clock> Timer::m_start = std::chrono::system_clock::now();
std::chrono::time_point<std::chrono::system_clock> Timer::m_stop = std::chrono::system_clock::now();

// straight forward class representing matrix
class Matrix {
public:
    static Matrix create(size_t const nRows, size_t const nColumns, bool const init = true)
    {
        auto result = Matrix(nRows, nColumns);
        if (!init) {
            return result;
        }

        auto size = result.getSize();
        for (size_t i = 0; i < size; ++i) {
            result[i] = Random::getFloat();
        }
        return result;
    }

    static Matrix copy(Matrix const &orig)
    {
        auto result = Matrix(orig.m_nRows, orig.m_nRows);
        auto size = result.getSize();
        for (size_t i = 0; i < size; ++i) {
            result[i] = orig[i];
        }
        return result;
    }

    size_t getSize() const
    {
        return m_nColumns * m_nRows;
    }

    float& operator[](size_t const idx)
    {
        return m_data[idx];
    }

    float const &operator[](size_t const idx) const
    {
        return m_data[idx];
    }

    size_t m_nRows = 0;
    size_t m_nColumns = 0;

private:
    Matrix(size_t const nRows, size_t const nColumns):
        m_nRows{ nRows },
        m_nColumns{ nColumns },
        m_data{ std::make_unique<float[]>(nRows * nColumns) }
    {}

    std::unique_ptr<float[]> m_data;

public:
    friend Matrix matmulBlocks(Matrix &a, Matrix &b, int chunkSize);
};

// helper function to compare matricies between each other
float matDiff(Matrix const &a, Matrix const &b)
{
    if (a.m_nRows != b.m_nRows || a.m_nColumns != b.m_nColumns) {
        throw std::invalid_argument("dimensions not compatible");
    }

    auto const size = a.getSize();

    float sum_delta = 0, sum = 0;
    for (auto i = 0u; i < size; ++i) {
        sum_delta += std::abs(a[i] - b[i]);
        sum += std::abs(a[i]) + std::abs(b[i]);
    }
    return sum_delta / sum;
}

// dumb way to multiply matricies
Matrix matmulSimple(Matrix const &a, Matrix const &b, int chunkSize)
{
    if (a.m_nColumns != b.m_nRows) {
        throw std::invalid_argument("dimensions not compatible");
    }

    auto const resNRows = a.m_nRows;
    auto const resNColumns = b.m_nColumns;

    auto res = Matrix::create(resNRows, resNColumns, false);

    auto const multLen = a.m_nColumns;
    #pragma omp parallel for default(shared) schedule(dynamic, chunkSize)
    for (auto r = 0u; r < resNRows; ++r) {
        for (auto c = 0u; c < resNColumns; ++c) {
            res[r * resNColumns + c] = 0;
            for (auto i = 0u; i < multLen; ++i) {
                res[r * resNColumns + c] += a[r * multLen + i] * b[i * multLen + c];
            }
        }
    }
    return res;
}

// less dumb way to multiply matricies
Matrix matmulByRows(Matrix const &a, Matrix const &b, int chunkSize)
{
    if (a.m_nColumns != b.m_nRows) {
        throw std::invalid_argument("dimensions not compatible");
    }

    auto const resNRows = a.m_nRows;
    auto const resNColumns = b.m_nColumns;

    auto res = Matrix::create(resNRows, resNColumns, false);

    auto const multLen = a.m_nColumns;
    #pragma omp parallel for default(shared) schedule(dynamic, chunkSize)
    for (auto r = 0u; r < resNRows; ++r) {
        for (auto c = 0u; c < resNColumns; ++c) {
            res[r * resNColumns + c] = 0;
        }
        for (auto i = 0u; i < multLen; ++i) {
            for (auto c = 0u; c < resNColumns; ++c) {
                res[r * resNColumns + c] += a[r * multLen + i] * b[i * resNColumns + c];
            }
        }
    }

    return res;
}

// cache-friendly way to multiply matricies
Matrix matmulBlocks(Matrix& a, Matrix& b, int chunkSize)
{
    if (a.m_nColumns != b.m_nRows) {
        throw std::invalid_argument("dimensions not compatible");
    }

    auto const resNRows = a.m_nRows;
    auto const resNColumns = b.m_nColumns;
    auto const multLen = a.m_nColumns;

    if (!((resNRows % 4 == 0) && (resNColumns % 4 == 0) && (multLen % 4 == 0))) {
        throw std::invalid_argument("can't divide matricies into blocks");
    }

    auto const nBlocksRow = resNRows / 4;
    auto const nBlocksColumn = resNColumns / 4;
    auto const blocksMultLen = multLen / 4;

    struct float4_t {
        float data[4];
        float &operator[](size_t idx)
        {
            return data[idx];
        }

        float const &operator[](size_t idx) const
        {
            return data[idx];
        }

        float4_t operator * (float v) const
        {
            float4_t res;
            for (auto i = 0u; i < 4; ++i) {
                res[i] = v * data[i];
            }
            return res;
        }

        float4_t operator + (float4_t const &rhs) const
        {
            float4_t res;
            for (auto i = 0u; i < 4; ++i) {
                res[i] = rhs[i] + data[i];
            }
            return res;
        }
    };

    auto const blockReorderLhs = [&nBlocksRow, &nBlocksColumn, &blocksMultLen, &chunkSize](float4_t *in, float4_t *out) {
        for (auto br = 0u; br < nBlocksRow; ++br) {
            for (auto bc = 0u; bc < blocksMultLen; ++bc) {
                float4_t *offs = in + bc + 4 * blocksMultLen * br;
                for (auto i = 0u; i < 4; ++i)
                    *(out++) = offs[i * blocksMultLen];
            }
        }
    };

    auto const blockReorderRhs = [&nBlocksRow, &nBlocksColumn, &blocksMultLen, &chunkSize](float4_t *in, float4_t *out) {
        for (auto bc = 0u; bc < nBlocksColumn; ++bc) {
            for (auto br = 0u; br < blocksMultLen; ++br) {
                float4_t *offs = in + bc + 4 * nBlocksColumn * br;
                for (auto i = 0u; i < 4; ++i)
                    *(out++) = offs[i * nBlocksColumn];
            }
        }
    };

    auto const blockReorderRes = [&nBlocksRow, &nBlocksColumn, &blocksMultLen, &chunkSize](float4_t *in, float4_t *out) {
        for (auto bc = 0u; bc < nBlocksColumn; ++bc) {
            for (auto br = 0u; br < nBlocksRow; ++br) {
                float4_t *offs = out + bc + 4 * nBlocksColumn * br;
                for (auto i = 0u; i < 4; ++i)
                    offs[i * nBlocksColumn] = *(in++);
            }
        }
    };

    auto tmpLhs = Matrix::create(a.m_nColumns, a.m_nRows, false);
    blockReorderLhs(reinterpret_cast<float4_t *>(a.m_data.get()), reinterpret_cast<float4_t *>(tmpLhs.m_data.get()));

    auto tmpRhs = Matrix::create(b.m_nColumns, b.m_nRows, false);
    blockReorderRhs(reinterpret_cast<float4_t *>(b.m_data.get()), reinterpret_cast<float4_t *>(tmpRhs.m_data.get()));

    auto tmpRes = Matrix::create(resNRows, resNColumns, false);
    #pragma omp parallel for default(shared) schedule(dynamic, chunkSize)
    for (auto br = 0u; br < nBlocksRow; ++br) {
        for (auto bc = 0u; bc < nBlocksColumn; ++bc) {
            auto r_ptr = reinterpret_cast<float4_t *>(tmpLhs.m_data.get() + 16 * blocksMultLen * br);
            auto c_ptr = reinterpret_cast<float4_t *>(tmpRhs.m_data.get() + 16 * blocksMultLen * bc);
            auto res_ptr = reinterpret_cast<float4_t *>(tmpRes.m_data.get() + 16 * (br + bc * nBlocksRow));

            float4_t tmp[4], tmp2[4];
            for (auto i = 0u; i < 4; ++i) {
                tmp[i] = {0, 0, 0, 0};
            }

            for (auto i = 0u; i < 4 * blocksMultLen; i += 4) {
                for (auto j = 0u; j < 4; ++j) {
                    tmp2[j] = c_ptr[i + j];
                }
                for (auto j = 0u; j < 4; ++j) {
                    for (auto k = 0u; k < 4; ++k) {
                        tmp[j] = tmp[j] + tmp2[k] * r_ptr[i + j][k];
                    }
                }
            }

            for (auto i = 0u; i < 4; ++i) {
                res_ptr[i] = tmp[i];
            }
        }
    }

    auto res = Matrix::create(resNRows, resNColumns, false);
    blockReorderRes(reinterpret_cast<float4_t *>(tmpRes.m_data.get()), reinterpret_cast<float4_t *>(res.m_data.get()));
    return res;
}

int main([[maybe_unused]] int const argc, [[maybe_unused]] char const *const argv[])
{
    // set dynamic to false just in case if implementation will allocate different number of threads
    // on every other run
    omp_set_dynamic(static_cast<int>(false));

    // output maximum available number of threads
    auto const maxThreads = omp_get_max_threads();
    std::printf("Max threads %d\n", maxThreads);

    auto const matDimSize = 1024;

    // create two square matricies with random floats
    auto a = Matrix::create(matDimSize, matDimSize);
    auto b = Matrix::create(matDimSize, matDimSize);

    // iterate chunk sizes for `omp schedule(dynamic, chunk size)`
    for (unsigned i = 1; i <= matDimSize; i *= 2) {
        auto timeSimple = 0;
        Timer::start();
        auto resSimple = matmulSimple(a, b, i);
        timeSimple = Timer::stop();

        auto timeByRows = 0;
        Timer::start();
        auto resByRows = matmulByRows(a, b, i);
        timeByRows = Timer::stop();

        auto timeBlocks = 0;
        Timer::start();
        auto resBlocks = matmulBlocks(a, b, i);
        timeBlocks = Timer::stop();

        std::printf("Chunk size %d\n"
            "\tSimple %d ms.\n"
            "\tByRows %d ms. diff with simple %f\n"
            "\tByBlocks %d ms. diff with simple %f\n",
            i, timeSimple, timeByRows, matDiff(resSimple, resByRows), timeBlocks, matDiff(resSimple, resBlocks));
    }

    // Example output from my machine:
    // Chunk size 1
    //         Simple 7635 ms.
    //         ByRows 6442 ms. diff with simple 0.000000
    //         ByBlocks 3753 ms. diff with simple 0.000000
    // Chunk size 2
    //         Simple 7983 ms.
    //         ByRows 6367 ms. diff with simple 0.000000
    //         ByBlocks 3881 ms. diff with simple 0.000000
    // Chunk size 4
    //         Simple 8518 ms.
    //         ByRows 6585 ms. diff with simple 0.000000
    //         ByBlocks 3986 ms. diff with simple 0.000000
    // Chunk size 8
    //         Simple 8735 ms.
    //         ByRows 6680 ms. diff with simple 0.000000
    //         ByBlocks 4027 ms. diff with simple 0.000000
    // Chunk size 16
    //         Simple 8813 ms.
    //         ByRows 6622 ms. diff with simple 0.000000
    //         ByBlocks 3992 ms. diff with simple 0.000000
    // Chunk size 32
    //         Simple 8623 ms.
    //         ByRows 6607 ms. diff with simple 0.000000
    //         ByBlocks 3975 ms. diff with simple 0.000000
    // Chunk size 64
    //         Simple 8575 ms.
    //         ByRows 6573 ms. diff with simple 0.000000
    //         ByBlocks 4488 ms. diff with simple 0.000000
    // Chunk size 128
    //         Simple 8434 ms.
    //         ByRows 6597 ms. diff with simple 0.000000
    //         ByBlocks 7509 ms. diff with simple 0.000000
    // Chunk size 256
    //         Simple 10442 ms.
    //         ByRows 7218 ms. diff with simple 0.000000
    //         ByBlocks 13929 ms. diff with simple 0.000000
    // Chunk size 512
    //         Simple 17502 ms.
    //         ByRows 11857 ms. diff with simple 0.000000
    //         ByBlocks 13739 ms. diff with simple 0.000000
    // Chunk size 1024
    //         Simple 37003 ms.
    //         ByRows 22166 ms. diff with simple 0.000000
    //         ByBlocks 13676 ms. diff with simple 0.000000


    // 1) obviously chunk size equal to whole job (1024) will effectively make this program single-threaded.
    //    this is the least efficient run.
    // 2) chunk sizes 4-64 is middle-ground. They are reasonably effective for each multiplication algorithm.
    // 3) the most effective chunk size is 1 due to the fact that multiplying one row is big-enough task by itself.

    return 0;
}
